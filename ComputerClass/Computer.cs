using System;
using System.Collections.Generic;
using ComputerClass.Components;
using ComputerClass.Motherboard;
using ComputerClass.Motherboard.MotherBoard;

namespace ComputerClass
{
    public class Computer
    {
        private MotherBoard _motherboard;
        private bool _isOn = false;
        private int _numberInClass;
        private AUserApplication _userApplication;

        public MotherBoard GetMotherBoard
        {
            get { return _motherboard; }
        }
        
        public bool GetIsOn
        {
            get { return _isOn; }
        }

        public int NumberInClass
        {
            get { return _numberInClass; }
            set { _numberInClass = value; }
        }

        public Computer(MotherBoard motherboard)
        {
            _motherboard = motherboard;
        }

        public void ComputerOn()
        {
            if (!_isOn)
            {
                _isOn = true;
                Console.WriteLine("Computer " + _numberInClass + " is on");
            }
            else
            {
                Console.WriteLine("Computer " + _numberInClass + " is already running");
            }
        }

        public void ComputerOff()
        {
            if (_isOn)
            {
                _isOn = false;
                Console.WriteLine("Computer " + _numberInClass + " is of");
            }
            else
            {
                Console.WriteLine("Computer " + _numberInClass + " is not turned on");
            }
        }

        public void Reboot()
        {
            if (_isOn)
            {
                Console.WriteLine("Computer " + _numberInClass + " restarted");
            }
            else
            {
                Console.WriteLine("Computer " + _numberInClass + " is not turned on");
            }
        }

        public void InstallUserApplication(AUserApplication userApplication)
        {
            _userApplication = userApplication;
            Console.WriteLine("User Application " + userApplication.GetName + " installed");
        }

        public void ChangeMotheBoard(MotherBoard motherBoard)
        {
            _motherboard = motherBoard;
            Console.WriteLine("On the computer motherboard is replaced");
        }
    }
}