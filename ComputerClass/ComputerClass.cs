﻿using System;
using System.Collections.Generic;
using ComputerClass.Components;
using ComputerClass.Motherboard.MotherBoard;

namespace ComputerClass
{
    public class ComputerClass
    {
        private static List<Computer> _computers;
        private static AUserApplication[] _userApplications;
        private static int _menuNumber = 1;
        private static PullComponents _pullComponents;

        static void Main()
        {
            _pullComponents = new PullComponents();

            PullUserApplications();
            PullComputers();
            MainMenu();
        }

        private static void MainMenu()
        {
            while (_menuNumber != 0)
            {
                Console.WriteLine("Select a menu item");
                Console.WriteLine("0 - Exit");
                Console.WriteLine("1 - On Computer");
                Console.WriteLine("2 - Off Computer");
                Console.WriteLine("3 - Reboot Computer");
                Console.WriteLine("4 - Install User Application");
                Console.WriteLine("5 - Sell Computer");
                Console.WriteLine("6 - Change MotheBoard");
                Console.WriteLine("7 - To Scrap");
                _menuNumber = int.Parse(Console.ReadLine());

                switch (_menuNumber)
                {
                    case 1:
                        ChooseComputer().ComputerOn();
                        break;
                    case 2:
                        ChooseComputer().ComputerOff();
                        break;
                    case 3:
                        ChooseComputer().Reboot();
                        break;
                    case 4:
                    {
                        Computer comp = ChooseComputer();

                        if (!comp.GetIsOn)
                        {
                            Console.WriteLine("Сomputer is off, first turn it on");
                            break;
                        }

                        comp.InstallUserApplication(ChooseUserApplication());
                        break;
                    }
                    case 5:
                        SellComputer();
                        break;
                    case 6:
                        ChooseComputer().ChangeMotheBoard(GetMotheBoard());
                        break;
                    case 7:
                        ToScrap();
                        break;
                    default:
                        Console.WriteLine("Not chosen");
                        break;
                }
            }
        }

        private static void PullComputers()
        {
            Console.WriteLine("Specify the number of computers in the classroom");
            int amount = int.Parse(Console.ReadLine());

            _computers = new List<Computer>();

            for (int i = 0; i < amount; i++)
            {
                Computer comp = new Computer(GetMotheBoard());
                comp.NumberInClass = i + 1;
                _computers.Add(comp);
            }
        }


        private static void PullUserApplications()
        {
            _userApplications = new AUserApplication[3];

            _userApplications[0] = new Ver01UserApplication("teacher");
            _userApplications[1] = new Ver02UserApplication("student");
            _userApplications[1].SetVersion(1);
            _userApplications[2] = new Ver02UserApplication("student");
            _userApplications[2].SetVersion(2);
        }

        private static Computer ChooseComputer()
        {
            int choice;

            Console.WriteLine("Available computers");
            foreach (var comp in _computers)
            {
                Console.WriteLine(comp.NumberInClass);
            }

            while (true)
            {
                Console.WriteLine("Enter computer number");
                choice = int.Parse(Console.ReadLine());
                foreach (var comp in _computers)
                {
                    if (comp.NumberInClass == choice)
                    {
                        return comp;
                    }
                }

                Console.WriteLine("There is no such computer");
            }
        }

        private static AUserApplication ChooseUserApplication()
        {
            int choice;

            while (true)
            {
                Console.WriteLine("Choose User Application");
                for (int i = 0; i < _userApplications.Length; i++)
                {
                    Console.WriteLine((i + 1) + " - " + _userApplications[i].GetName);
                }

                choice = int.Parse(Console.ReadLine()) - 1;

                if (choice < 0 || choice >= _userApplications.Length)
                {
                    Console.WriteLine("No such program");
                    continue;
                }

                return _userApplications[choice];
            }
        }

        private static void SellComputer()
        {
            Computer comp = ChooseComputer();
            MotherBoard motherBoard = comp.GetMotherBoard;
            float money = motherBoard.GetSpecification.Price;
            foreach (var component in motherBoard.GetComponents)
            {
                money += component.GetSpecification.Price;
            }
            
            Console.WriteLine("Computer " + comp.NumberInClass + " sold, earned money: " + money);
            _computers.Remove(comp);
        }

        private static MotherBoard GetMotheBoard()
        {
            MotherBoard motherBoard;
            motherBoard = _pullComponents.GetRandomMotheboard();
            motherBoard.AddComponents
            (
                _pullComponents.GetRandomHardDisk(),
                _pullComponents.GetRandomMemory(),
                _pullComponents.GetRandomVideoCard()
            );
            return motherBoard;
        }

        private static void ToScrap()
        {
            Computer comp = ChooseComputer();
            MotherBoard motherBoard = comp.GetMotherBoard;
            float weight = motherBoard.GetSpecification.Weight;
            foreach (var component in motherBoard.GetComponents)
            {
                weight += component.GetSpecification.Weight;
            }
            
            Console.WriteLine("Computer " + comp.NumberInClass + " scrapped, weight: " + weight);
            _computers.Remove(comp);
        }
    }
}