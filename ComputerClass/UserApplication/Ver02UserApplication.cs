namespace ComputerClass
{
    public class Ver02UserApplication : AUserApplication
    {
        public Ver02UserApplication(string name) : base(name)
        {
            _name = name;
        }

        public override void SetVersion(int version)
        {
            _name = _name + " " + version;
        }
    }
}