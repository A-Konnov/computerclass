namespace ComputerClass
{
    public abstract class AUserApplication
    {
        protected string _name;
        protected int _version;

        public string GetName
        {
            get { return _name; }
        }
        
        public int GetVersion
        {
            get { return _version; }
        }
        

        public AUserApplication(string name)
        {
            _name = name;
        }

        public abstract void SetVersion(int version);
    }
}