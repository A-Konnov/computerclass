using System;
using ComputerClass.Motherboard;
using ComputerClass.Motherboard.MotherBoard;

namespace ComputerClass.Components
{
    public class PullComponents
    {
        private MotherBoard[] _motherboards;
        private HardDisk[] _hards;
        private Memory[] _memorys;
        private VideoCard[] _videos;
        private Random _rand = new Random();

        public MotherBoard GetRandomMotheboard()
        {
            int i = _rand.Next(0, _motherboards.Length - 1);
            return _motherboards[i];
        }
        
        public HardDisk GetRandomHardDisk()
        {
            int i = _rand.Next(0, _hards.Length - 1);
            return _hards[i];
        }
        
        public Memory GetRandomMemory()
        {
            int i = _rand.Next(0, _memorys.Length - 1);
            return _memorys[i];
        }
        
        public VideoCard GetRandomVideoCard()
        {
            int i = _rand.Next(0, _videos.Length - 1);
            return _videos[i];
        }
        

        public PullComponents()
        {
            _hards = new HardDisk[3];
            _hards[0] = new HardDisk(new Specification("WD", 200f, 10f));
            _hards[1] = new HardDisk(new Specification("Seagate", 250f, 12f));
            _hards[2] = new HardDisk(new Specification("Toshiba", 190f, 9f));
            
            _memorys = new Memory[2];
            _memorys[0] = new Memory(new Specification("HyperX", 350f, 5f));
            _memorys[1] = new Memory(new Specification("Crucial", 290f, 4f));
            
            _videos = new VideoCard[2];
            _videos[0] = new VideoCard(new Specification("Gigabyte", 400f, 9f));
            _videos[1] = new VideoCard(new Specification("Palit", 290f, 8f));
            
            _motherboards = new MotherBoard[3];
            _motherboards[0] = new MotherBoard(new Specification("Gigabyte", 150f, 12f));
            _motherboards[1] = new MotherBoard(new Specification("ASUS", 140f, 13f));
            _motherboards[2] = new MotherBoard(new Specification("MSI", 120f, 12f));
        }
    }
}