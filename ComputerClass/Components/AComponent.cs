namespace ComputerClass.Motherboard
{
    public abstract class AComponent
    {
        protected Specification _specification;

        public Specification GetSpecification
        {
            get { return _specification; }
        }

        public AComponent(Specification spec)
        {
            _specification = spec;
        }
    }
}