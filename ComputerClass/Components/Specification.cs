namespace ComputerClass.Motherboard
{
    public struct Specification
    {
        public string Name;
        public float Price;
        public float Weight;

        public Specification(string name, float price, float weight)
        {
            Name = name;
            Price = price;
            Weight = weight;
        }
    }
}