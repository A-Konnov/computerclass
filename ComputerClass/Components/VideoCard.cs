namespace ComputerClass.Motherboard
{
    public class VideoCard : AComponent
    {
        public VideoCard(Specification spec) : base(spec)
        {
            _specification = spec;
        }
    }
}