using System.Collections.Generic;

namespace ComputerClass.Motherboard.MotherBoard
{
    public class MotherBoard : AComponent
    {
        private List<AComponent> _components;

        public List<AComponent> GetComponents
        {
            get { return _components; }
        }
        
        public MotherBoard(Specification spec) : base(spec)
        {
            _specification = spec;
            
            _components = new List<AComponent>();
        }

        public void AddComponents(AComponent hard, AComponent memory, AComponent video)
        {
            _components.Add(hard);
            _components.Add(memory);
            _components.Add(video);
        }
    }
}